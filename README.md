# 20/12 update
What has changed?
- redux for state management
- A site with statistic for Vietnam
- Support for English & Vietnamese
- Dark mode
![Screenshot](public/demo.gif)
# Covid Tracker and News
A simple covid tracking app. Build with:
- Reactjs + TypeScript
- Material UI
- apexchart
- covid data from [here](https://disease.sh/)
- news data from vnexpress

![Screenshot](public/screenshot.png)