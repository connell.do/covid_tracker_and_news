import { makeStyles } from "@material-ui/core"
import {theme} from "./utils/types"


export interface StyleProps {
theme: theme
}
const useStyles = makeStyles(theme => ({
    header: {
      backgroundColor: (props: StyleProps) => {
          return (props.theme === "light")? "rgb(245,245,245)": "rgb(38, 43, 54)"
        },
      color:  (props: StyleProps) => {
          return (props.theme === "light")? "black": "white"
        }
    },
    app__stats: {
        padding: "20px",
        display: "flex",
        justifyContent: "space-evenly",
        marginTop: "50px",
        [theme.breakpoints.up('md')]: {
          "columnGap": "12em",
        },
    },
    App: {
        display: "flex",
        justifyContent: "space-evenly",
        transition: "all 0.30s linear",
        backgroundColor: (props: StyleProps) => {
            return (props.theme==="light")? "rgb(233, 233, 233)": "rgb(24, 31, 48)";
        },
        color: (props: StyleProps) => {
            return (props.theme==="light")? "#000": "rgb(233, 233, 233)";
        },
        [theme.breakpoints.down('md')]: {
            flexDirection: "column"
          },
        [theme.breakpoints.up('md')]: {
            flexDirection: "row"
          },
      },
    app__left: {
        flex: 1,
      },
      
    app__news: {
        marginTop: "100px",
        display: "flex",
        justifyContent: "center"
      },
    app__news__content: {
        width: "70%",
      },
    app__right :{
        backgroundColor: (props: StyleProps) => {
          return (props.theme==="light")? "rgb(233, 233, 233)": "rgb(24, 31, 48)";
      },
      color: (props: StyleProps) => {
        return (props.theme==="light")? "#000": "rgb(233, 233, 233)";
      },
      [theme.breakpoints.up('md')]: {
          maxWidth: "25%",
      },
        [theme.breakpoints.down('md')]: {
            width: "80%",
            border: "black",
            alignSelf: "center"
        } 
      }

}))

export default useStyles