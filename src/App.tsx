// import './App.css';
import { Card, CardContent, Typography } from '@material-ui/core';
import { useEffect, useState } from 'react';
import InfoBox from './components/InfoBox/InfoBox';
import Table, {ITableRow} from "./components/SideTable/Table";
import LineGraph from './components/SideGraphs/SideGraphs';
import NewsList from './components/NewsList/NewsList';
import Header from './components/Header/Header';
import { translate } from './lang';
import { RootState } from './store/store';
import useStyles, {StyleProps} from './AppStyle';
import {lang, mode, theme} from "./utils/types"
import { useSelector } from 'react-redux';
import {
  COVID_CASES_WORLD_WIDE, 
  COVID_CASES_ALL_COUNTRIES, 
  COVID_CASES_VIETNAM,
  COVID_VACCINE_VIETNAM,
  COVID_CASES_PROVINCE
} from "./utils/apis"
import CityGraph from './components/CityGraph/CityGraph';
// import VaccineBar from "./components/VaccineBar/VaccineBar"
interface ISelectionInfo {
  todayCases: Number,
  cases: Number,
  todayDeaths: Number,
  deaths: Number,
  recovered: Number,
  todayRecovered: Number
}


function App() {
  const language: lang = useSelector((state: RootState) => state.lang.language)
  const theme: theme = useSelector((state: RootState) => state.theme.theme)
  const currentMode: mode = useSelector((state: RootState) => state.mode.mode)

  const [infoOfSelection, setInfoOfSelection] = useState<ISelectionInfo>({
    todayCases: 0,
    cases: 0,
    todayDeaths: 0,
    deaths: 0,
    recovered: 0,
    todayRecovered: 0
  });
  const [noOneShot, setNoOneShot] = useState(0);
  const [noTwoShot, setNoTwoShot] = useState(0);
  const AppStyleProp : StyleProps = {
    theme: theme
  }
  const styles = useStyles(AppStyleProp);
  const [tableDataForCountries, setTableDataForCountries] = useState<Array<ITableRow>>([]);
  const [tableDataForProvinces, setTableDataForProvinces] = useState<Array<ITableRow>>([]);
  useEffect(() => {
    const getCountries = async () => {
      // Convert to axios?
      await fetch(COVID_CASES_ALL_COUNTRIES)
      .then((response) => response.json())
      .then((data: any) => {
        const tableData: Array<ITableRow> = 
          data.map((country: any) => ({
            country: country.country,
            cases: country.cases
          } as ITableRow))
          setTableDataForCountries(tableData);
      })
    }
    getCountries()
  }, [])

  useEffect(() => {
    fetch(COVID_CASES_WORLD_WIDE)
    .then((response => response.json()))
    .then((data) => 
    setInfoOfSelection({
      todayCases: data.todayCases,
      cases: data.cases,
      todayDeaths: data.todayDeaths,
      deaths: data.deaths,
      todayRecovered: data.todayRecovered,
      recovered: data.recovered,
    } ))
  },[])
  useEffect(() => {
    const getCountries = async () => {
      // Convert to axios?
      await fetch(COVID_CASES_PROVINCE)
      .then((response) => response.json())
      .then((data: any) => {
        const tableData: Array<ITableRow> = 
          data['data']['cases'].map((country: any) => ({
            country: country.x,
            cases: country.y
          } as ITableRow))
          setTableDataForProvinces(tableData);
      })
    }
    getCountries()
  }, [])
  useEffect(() => {
    const getCountries = async () => {
      await fetch(COVID_VACCINE_VIETNAM)
      .then((response) => response.json())
      .then((data: any) => {
        setNoOneShot(data["data"]['first']['total'])
        setNoTwoShot(data["data"]['second']['total'])
      })
    }
    getCountries()
  }, [])
  const onTabSelection = async (newSelection: mode) => {
    const url = getUrl(newSelection)
    await fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setInfoOfSelection({
          todayCases: data.todayCases,
          cases: data.cases,
          todayDeaths: data.todayDeaths,
          deaths: data.deaths,
          todayRecovered: data.todayRecovered,
          recovered: data.recovered,
        } )
      })
  }

  const getUrl = (countryCode: mode) => {
    let url = ''
    if (countryCode === 'worldwide')
      url = COVID_CASES_WORLD_WIDE
    else
      url = COVID_CASES_VIETNAM
    return url
  }


  const renderHeader = () => {
    return <Header 
    handleTabSelection={onTabSelection}/>
  }
  
  const renderStatisticBox = () => {
    return (<>
      <div className={styles.app__stats}>
          <InfoBox 
            title={translate("totalCases", language)}
            cases={infoOfSelection.todayCases} 
            total={infoOfSelection.cases}
            primaryColor={"rgb(233,162,59)"}
          />
          <InfoBox 
            title={translate("totalRecoveries", language)}
            cases={infoOfSelection.todayRecovered} 
            total={infoOfSelection.recovered}
            primaryColor={"rgb(77,165,121)"}
          />
          <InfoBox 
            title={translate("totalDeaths", language)}
            cases={infoOfSelection.todayDeaths} 
            total={infoOfSelection.deaths}
            primaryColor={"rgb(221,82,76)"}
          />
          </div>
          </>
    )
  }
  const renderVaccineBox = () => {
    return (
      <div className={styles.app__stats} style={{margin: 0}}>
        <InfoBox 
        title={translate("totalJabbedPeople", language)}
        cases={noOneShot+noTwoShot} 
        total={null}
        primaryColor="rgb(233,162,59)"
      />
      <InfoBox 
        title={translate("totalOneJabbedPeople", language)}
        cases={noOneShot} 
        total={null}
        primaryColor={"rgb(77,165,121)"}
      />
      <InfoBox 
        title={translate("totalTwoJabbedPeople", language)}
        cases={noTwoShot} 
        total={null}
        primaryColor={"rgb(221,82,76)"}
      />
      </div>
    )}
  const renderNewsList = () => {
    return <div className={styles.app__news}>
      <div className={styles.app__news__content}>
        <NewsList/>
      </div> 
    </div>
  }
  return (
    <div className={styles.App}>
      <div className={styles.app__left}>
        {renderHeader()}
        {renderStatisticBox()}
        <div style={{marginTop: "50px"}}>
        {currentMode==="vietnam" && <CityGraph/>}
        </div>
        {currentMode === "vietnam" && <>
          <div style={{display: "flex", justifyContent:"center", marginTop: "20px"}}>
          <Typography variant="h4" color='primary'>
                <b>{translate("vaccineStatistic",language)}</b>
            </Typography>
          </div>
          {renderVaccineBox()}
          </>
          }
          {renderNewsList()}
      </div>
      <Card classes={{ root: styles.app__right }} >
          <CardContent>
            <Table data={currentMode === "worldwide"? tableDataForCountries: tableDataForProvinces}/>
          </CardContent>
          <div style={{marginTop: "10px"}}></div>
          <LineGraph/>
      </Card>
    </div>
  );

}

export default App;
