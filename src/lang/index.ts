import { lang } from '../utils/types';
import en from './en.json';
import vn from './vn.json';

export const translate = (key: string, language: lang): string => {
  let langData: { [key: string]: string } = {};

  if(language === 'en') {
    langData = en;
  }else if(language === 'vn') {
    langData = vn;
  }
  

  return langData[key];
}
