// import './Table.css'
import {useEffect, useState} from "react"
import FilterListIcon from '@mui/icons-material/FilterList';
import IconButton from "@material-ui/core/IconButton";
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import { translate } from '../../lang';
import useStyles from "./TableStyles";


export interface ITableRow {
    country: String,
    cases: Number
  };
  
function Table({data}: {data: Array<ITableRow>}) {
    type sortType = "sort-s-l" | "sort-l-s" | "unsort";
    const nextSortType = {
        "unsort": "sort-l-s"   as sortType,
        "sort-l-s": "sort-s-l" as sortType,
        "sort-s-l": "sort-l-s" as sortType
    }
    const [sortStatus, setSortStatus] = useState<sortType>("unsort")
    const [tableData, setTableData] = useState<Array<ITableRow>>([])
    useEffect(() => {
        setTableData(data)
    }, [data])
    const language = useSelector((state: RootState) => state.lang.language)
    const styles = useStyles();
    
    const sortData = (order: sortType) => {
        let sortData = [...tableData]
        if (order === "sort-l-s"){
            sortData.sort((a, b) => {
                if (a.cases < b.cases) {
                    return -1;
                }
                return 1;
            })
        }
        else if (order === "sort-s-l"){
            sortData.sort((a, b) => {
                if (a.cases > b.cases) {
                    return -1;
                }
                return 1;
            })
        }
        setTableData(sortData)
    }
    const sortDataByType = (order: sortType) => {
        sortData(order);
        setSortStatus(order);
    }

    const handleSort = () => {
        const nextType: sortType = nextSortType[sortStatus];
        sortDataByType(nextType);
    }

    return (
        <>
    <tr>
        <td key="title"><b><h3>{translate("listOfCasesTitle", language)}</h3></b></td>
        <td key="filtericon"><IconButton aria-label="Sort" onClick={handleSort} >
            <FilterListIcon />
        </IconButton></td>
    </tr>
    <div className={styles.table}>
    {tableData.map(({country, cases}) => (
            <tr className={styles["table-row"]}>
                <td key={country as string} className={styles["table-data"]}><b>{country}</b></td>
                <td key={country + "-cases"} className={styles["table-data"]}>{cases}</td>
            </tr>
        ))}
    </div>
    </>)
}


export default Table;