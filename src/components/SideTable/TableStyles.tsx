import { makeStyles } from "@material-ui/core"

const useStyles = makeStyles(theme => ({
    table: {
        marginTop: "20px",
        overflow: "scroll",
        height: "400px",
    },
    "table-row": {
        display: "flex",
        justifyContent: "space-between",
    },
    "table-data": {
        padding: "0.5rem"
    }
}))


export default useStyles