
import FormControl from '@material-ui/core/FormControl';
import {Tabs, Tab} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, AppDispatch } from '../../store/store';
import {translate} from '../../lang';
import ToggleButton from '@mui/material/ToggleButton';
import {setLanguage} from "../../slicer/LangSlice";
import {setTheme} from "../../slicer/ThemeSlice"
import LanguageIcon from '@mui/icons-material/Language';
import { getThemeClass } from '../../utils/theme';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import Brightness3Icon from '@mui/icons-material/Brightness3';
import "./Header.css"
import {mode,theme} from "../../utils/types"
import {setMode} from "../../slicer/ModeSlice"
interface Props {
    handleTabSelection: ((selection: mode) => void);
}


export default function Header({handleTabSelection}: Props) {
    const currentSelection = useSelector((state: RootState) => state.mode.mode);
    const dispatch: AppDispatch = useDispatch()
    const onTabSelection =   (event: any, newSelection: mode) => {
        dispatch(setMode())
        handleTabSelection(newSelection);
    }
    const language = useSelector((state: RootState) => state.lang.language)
    const theme: theme = useSelector((state: RootState) => state.theme.theme)


    return <div className={`app__header + ${getThemeClass('app__header', theme)}`}>
    <h1>{translate("title", language)}</h1>
        <FormControl className="position_form">
        <Tabs 
            value={currentSelection} 
            onChange={onTabSelection} 
            aria-label="basic tabs example">
            <Tab key={"worldwide" as mode} label={translate("worldwide", language)} value="worldwide"/>
            <Tab key={"vietnam" as mode} label={translate("vietnam", language)} value="vietnam"/>
        </Tabs>
        </FormControl>
        <ToggleButton
        value="check"
        onChange={() => {
            dispatch(setLanguage())
        }}
        >
            <LanguageIcon className={`lang-button + ${getThemeClass('lang-button', theme)}`}/>
            <div className={`lang-button + ${getThemeClass('lang-button', theme)}`}>{language}</div>
        </ToggleButton>
        <ToggleButton
        value="check"
        onChange={() => {
            dispatch(setTheme())
        }}
        sx={{border: "none"}}
        >
            <div className="lang-button">
                {theme==="light"? <WbSunnyIcon fontSize="medium" />: <Brightness3Icon fontSize="medium" sx={{color: "white"}}/>}
            </div>
        </ToggleButton>
    </div>
}