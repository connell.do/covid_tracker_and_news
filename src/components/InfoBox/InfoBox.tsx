import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import {theme} from "../../utils/types"
type InfoBoxProps = {
    title: string,
    cases: Number,
    total: Number|null,
    primaryColor: string
}
function InfoBox({title, cases, total, primaryColor}: InfoBoxProps){
    const theme: theme = useSelector((state: RootState) => state.theme.theme);
    let boxStyle = {
        width: 225,
        minWidth: 200,
        minHeight: 130,
        backgroundColor: (theme === "light")? "#FFF": "rgb(35, 43, 55)"
    }
    let typoColor = (theme === "light")? "#000": "#FFF";
    return (
        <Card sx={boxStyle}>
            <CardContent >
                <Typography variant="h5" color={typoColor} sx={{paddingBottom: "10px"}}>
                    <b>{title}</b>
                </Typography>
                <Typography variant="h5" sx={{paddingBottom: "10px"}} color={primaryColor}>
                    <b>{cases}</b>
                </Typography>
                <Typography variant="body1" color={typoColor}>
                    {total}
                </Typography>
            </CardContent>
        </Card>
    )
}


export default InfoBox;