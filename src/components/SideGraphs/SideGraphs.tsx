import React, { useState, useEffect } from "react";
import Chart, {} from "react-apexcharts";
import {useSelector} from "react-redux"
import { RootState } from "../../store/store";
import {theme} from "../../utils/types"
interface ICoordinate {
    x: String,
    y: Number
}
function LineGraph() {
    const[chartCaseData, setChartCaseData] = useState<ICoordinate[]>([])
    const[chartDeathData, setChartDeathData] = useState<ICoordinate[]>([])
    const[chartRecoveredData, setChartRecoveredData] = useState<ICoordinate[]>([])
    const theme: theme = useSelector((state: RootState) => state.theme.theme)

    const buildChartConfig = (theme: theme, title: string="") => {
        return {
            chart: {
              id: 'area-datetime',
              width: '100%',
              zoom: {
                enabled: false
              },
              toolbar: {
                  show: false
              },
              animations: {
                enabled: true,
                easing: 'easeinout' as const,
                speed: 800,
                animateGradually: {
                    enabled: true,
                    delay: 150
                },
                dynamicAnimation: {
                    enabled: true,
                    speed: 350
                }
            }
            },
            colors: [((theme ==="light")? '#2E93fA':'#F44336')],
            xaxis: {
                type: 'datetime'  as const,
                tickamount: 6,
                labels: {
                    style: {
                        colors: [((theme ==="light")? 'black':'white')]
                    }
                },
              },
            dataLabels: {
            enabled: false
            },
            yaxis: {
                tickamount: 5,
                labels: {
                    style: {
                        colors: [((theme ==="light")? 'black':'white')]
                    }
                },
            },
            title: {
                text: title,
                style: {
                    color: ((theme ==="light")? 'black':'white')
                }        
            },
            stroke: {
                width: 1
              }
        };  
    }
    const buildChartData = (data: any, type: string): Array<ICoordinate> => {
        const chartDataPoints: Array<ICoordinate> = []
        let previousDataPoint;
        let numberOfCasesByDate = data[type]
        for (const day in numberOfCasesByDate)
        {   
            if (previousDataPoint) {
                const newData: ICoordinate = {
                    x: day,
                    y: numberOfCasesByDate[day] - previousDataPoint
                }
                chartDataPoints.push(newData)
            }
            previousDataPoint = numberOfCasesByDate[day]
        }
        return chartDataPoints;
    }

    useEffect(() => {
        fetch("https://disease.sh/v3/covid-19/historical/all")
        .then (response => response.json())
        .then ((data: any) => {
            const chartCaseData = buildChartData(data, 'cases');
            const chartDeathData = buildChartData(data, 'deaths');
            const chartRecoveredData = buildChartData(data, 'recovered'); 
            setChartCaseData(chartCaseData)
            setChartDeathData(chartDeathData)
            setChartRecoveredData(chartRecoveredData)
        })
    }, [])
    const totalCaseConfig = buildChartConfig(theme, "Total Cases");
    const deathCasesConfig = buildChartConfig(theme, "Death Cases");
    const recoveredCasesConfig = buildChartConfig(theme, "Recovered Cases");
    return (
    <>
        <Chart
            type="area"
            series={[{data: chartCaseData}]}
            options={totalCaseConfig}
        />
        <Chart
            type="area"
            series={[{data: chartDeathData}]}
            options={deathCasesConfig}
        />    
        <Chart
            type="area"
            series={[{data: chartRecoveredData}]}
            options={recoveredCasesConfig}
        />
    </>)
    
}



export default LineGraph;