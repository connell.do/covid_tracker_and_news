import useStyles, {NewListStyleProps} from "./styles";

import {useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Button } from '@mui/material';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { timeSince } from '../../utils/util';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import { translate } from '../../lang';
import {theme} from "../../utils/types"
// import {useDarkHeader, useLightItem, useLightHeader, useDarkItem} from "./styles";
const VNEXPRESS_NEWS_2 =  " https://gw.vnexpress.net/ar/get_rule_2?category_id=1001005&limit=100&page=1&data_select=article_id,article_type,privacy,title,lead,share_url,thumbnail_url,new_privacy,publish_time,original_cate,off_thumb,iscomment&exclude_id=4332910";
interface article {
    lead: string;
    share_url: string;
    title: string;
    thumbnail_url: string;
    publish_time: string;
};
const defaultArticle: article = {
    lead: "",
    share_url: "",
    title: "",
    thumbnail_url: "/fallback.jpg",
    publish_time: "",
  }
function NewsList() {
    const [noNewsToDisplay, setNoNewsToDisplay] = useState<number>(4)
    const [news, setNews] = useState<Array<article>>([defaultArticle]);
    const language = useSelector((state: RootState) => state.lang.language)
    const theme: theme = useSelector((state: RootState) => state.theme.theme)
    const props:NewListStyleProps = {
        theme: theme
    }
    const styles = useStyles(props);
    const fetchNews = async () => {
        await fetch(VNEXPRESS_NEWS_2)
        .then((response) => {
            return response.json()
        })
        .then( data => {
            const keyId = Object.keys(data.data)[0];
            setNews(data.data[keyId].data);
        })
    };
    
    useEffect(() => {
        fetchNews();
    }, []);

    // useEffect((theme) => {
    //     if (theme === "light") {
    //         headerTheme = useLightHeader();
    //         itemTheme = useLightItem();
    //     }
    //     else {
    //         headerTheme = useLightHeader();
    //         itemTheme = useLightItem();
    //     }
    // }, [theme]);

    const handleExpand = () => {
        if (noNewsToDisplay >= news.length) {
            setNoNewsToDisplay(10);
        } else {
            setNoNewsToDisplay(noNewsToDisplay + 4);
        }
    }

    return (
        <div style={{ display: "inline-block" }}>
        <Card>
                <CardContent className={styles.header}>
                    <Typography className={styles.headerTitle} variant="h4">
                    {translate("newsTitle", language)}
                    </Typography>
                </CardContent>
        </Card>
        {
            news.slice(0, noNewsToDisplay).map((n) => {
                return <Card className={styles.newsCard}>
                <CardMedia
                  component="img"
                  sx={{width: 151}}
                  image={n.thumbnail_url}
                  alt="Image"
                />
                <CardActionArea href={n.share_url}>
                <Box className={styles.newsCardInner}>
                 <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography className={styles.newTitles}>
                        <b>{n.title}</b>
                    </Typography>
                    <Typography className={styles.newTitles}>
                        {n.lead}
                    </Typography>
                    <Typography className={styles.newTitles}>
                        {timeSince(n.publish_time)}
                    </Typography>
                </CardContent>
                </Box>
                </CardActionArea>
              </Card>
            })
        }
        <Button onClick={handleExpand}>{translate("expandButton", language)}</Button>
      </div>
    );
}

export default NewsList