import { makeStyles } from '@material-ui/core/styles';
import {theme} from "../../utils/types"

export interface NewListStyleProps {
  theme: theme
}
const useStyles = makeStyles({
  header: {
    display: "flex",
    backgroundColor: (props: NewListStyleProps) => {return (props.theme === "light")? "rgb(245,245,245)": "rgb(38, 43, 54)"},
    borderColor: (props: NewListStyleProps) => {return (props.theme === "light")? "rgb(38, 43, 54)": "black"},
    borderBottom: "0.51px solid",
  },
  headerTitle: {
    color:  (props: NewListStyleProps) => {return (props.theme === "light")? "black": "white"},
  },
  newsCard: {
    display: 'flex', 
    padding: 1,
    borderColor: (props: NewListStyleProps) => {return (props.theme === "light")? "rgb(38, 43, 54)": "black"},
    borderBottom: "0.51px solid",
  },
  newsCardInner: {
    border: "none", 
    backgroundColor: (props: NewListStyleProps) => {return (props.theme === "light")? "rgb(245,245,245)": "rgb(38, 43, 54)"},
    display: 'flex', 
    flexDirection: 'column',
  },
  newTitles: {
    color:  (props: NewListStyleProps) => {return (props.theme === "light")? "black": "white"},
  }
})

export default useStyles;