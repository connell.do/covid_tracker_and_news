import { useState, useEffect } from "react";
import Chart, {} from "react-apexcharts";
import {useSelector} from "react-redux"
import { RootState } from "../../store/store";
import {theme} from "../../utils/types"
import {
    COVID_CASES_HCMC,
    COVID_CASES_HANOI
} from "../../utils/apis"
import { translate } from '../../lang';
import { Button, ButtonGroup } from "@material-ui/core";

interface ICoordinate {
    x: String,
    y: Number
}
type city = "hanoi"|"hcm"
function CityGraph() {
    const theme: theme = useSelector((state: RootState) => state.theme.theme)
    const language = useSelector((state: RootState) => state.lang.language)

    const[chartHCMData, setChartHCMData] = useState<ICoordinate[]>([])
    const[chartHNData, setChartHNData] = useState<ICoordinate[]>([])
    const [choice, setChoice] = useState<city>("hcm")
    // const[chartVNData, setChartVNData] = useState<ICoordinate[]>([])
    const[chartHCMDataCumilative, setChartHCMDataCumilative] = useState<ICoordinate[]>([])
    const[chartHNDataCumilative, setChartHNDataCumilative] = useState<ICoordinate[]>([])
    // const[chartVNDataCumilative, setChartVNDataCumilative] = useState<ICoordinate[]>([])
    const buildChartConfig = (theme: theme, title: string="") => {
        return {
            chart: {
              id: 'area-datetime',
              width: '100%',
              zoom: {
                enabled: false
              },
              toolbar: {
                  show: false
              },
              animations: {
                enabled: true,
                easing: 'easeinout' as const,
                speed: 800,
                animateGradually: {
                    enabled: true,
                    delay: 150
                },
                dynamicAnimation: {
                    enabled: true,
                    speed: 350
                }
            }
            },
            colors: [((theme ==="light")? '#2E93fA':'#F44336')],
            xaxis: {
                type: 'datetime'  as const,
                tickamount: 6,
                labels: {
                    style: {
                        colors: [((theme ==="light")? 'black':'white')]
                    }
                }
              },
            dataLabels: {
            enabled: false
            },
            yaxis: {
                tickamount: 5,
                labels: {
                    style: {
                        colors: [((theme ==="light")? 'black':'white')]
                    }
                },
            },
            title: {
                text: title,
                align: "center" as const,
                style: {
                    fontSize:  '14px',
                    fontWeight:  'bold',
                    color: ((theme ==="light")? 'black':'white')
                }        
            },
            stroke: {
                width: 1
              }
            
        };  
    }

    useEffect(() => {
        const handleNumberWithDot = (x: Number)=>{
            let s: string = x.toString();
            s = s.replace(".","");
            return parseInt(s)
        }
        fetch(COVID_CASES_HCMC)
        .then (response => response.json())
        .then ((data: any) => {
            let hcmdata = data['data']['data'];
            // console.log(hcmdata)
            let pointData: ICoordinate[] = []
            let cummiData: ICoordinate[] = []
            for (const dat of hcmdata) {
                let new_point_data: ICoordinate = {
                    x: dat["date"],
                    y: handleNumberWithDot(dat["daily"])
                }
                let new_cum_data: ICoordinate = {
                    x: dat["date"],
                    y: handleNumberWithDot(dat["total"])
                }
                pointData.push(new_point_data)
                cummiData.push(new_cum_data)
            }
            setChartHCMData(pointData)
            setChartHCMDataCumilative(cummiData)
            console.log(cummiData)
        })
        fetch(COVID_CASES_HANOI)
        .then (response => response.json())
        .then ((data: any) => {
            let hndata = data['data']['data'];
            // console.log(hcmdata)
            let pointData: ICoordinate[] = []
            let cummiData: ICoordinate[] = []
            for (const dat of hndata) {
                let new_point_data: ICoordinate = {
                    x: dat["date"],
                    y: dat["daily"]
                }
                let new_cum_data: ICoordinate = {
                    x: dat["date"],
                    y: dat["total"]
                }
                pointData.push(new_point_data)
                cummiData.push(new_cum_data)
            }
            setChartHNData(pointData)
            setChartHNDataCumilative(cummiData)
            console.log(cummiData)
        })
        // fetch(COVID_CASES_VIETNAM2)
        // .then (response => response.json())
        // .then ((data: any) => {
        //     let hndata = data['data']['vn']['cases'];
        //     // console.log(hcmdata)
        //     let pointData: ICoordinate[] = []
        //     let cummiData: ICoordinate[] = []
        //     for (const dat of hndata) {
        //         let new_point_data: ICoordinate = {
        //             x: dat["date"],
        //             y: dat["daily"]
        //         }
        //         let new_cum_data: ICoordinate = {
        //             x: dat["date"],
        //             y: dat["total"]
        //         }
        //         pointData.push(new_point_data)
        //         cummiData.push(new_cum_data)
        //     }
        //     setChartHNData(pointData)
        //     setChartHNDataCumilative(cummiData)
        //     console.log(cummiData)
        // })
    }, [])
    return (
    <>
        <div style={{display: "flex", justifyContent:"center", width: "100%", }}>
            <div style={{width: "10%"}}>
            <ButtonGroup
                orientation="vertical"
                aria-label="vertical contained button group"
                variant="text"
            >
                <Button key="hcm" onClick={() => setChoice("hcm")}> <div style={{color: theme==="light"? "black":"white"}}>Ho Chi Minh</div></Button>
                <Button key="hn"  onClick={() => setChoice("hanoi")}><div style={{color: theme==="light"? "black":"white"}}>Ha Noi</div></Button>
            </ButtonGroup>
            </div>
            <div style={{width: "35%"}}>
            <Chart
            type="bar"
            series={choice==="hcm"? [{data: chartHCMData}]:[{data: chartHNData}]}
            options={buildChartConfig(theme, translate(((choice==="hcm")? "caseByDayInHCM": "caseByDayInHanoi"), language))}
        />
            </div>
            <div style={{width: "35%"}}>
            <Chart
            type="area"
            series={choice==="hcm"? [{data: chartHCMDataCumilative}]:[{data: chartHNDataCumilative}]}
            options={buildChartConfig(theme, translate(((choice==="hcm")? "totalCaseInHCM": "totalCaseInHanoi"), language))}
        />
            </div>
        </div>


    </>)
    
}



export default CityGraph;