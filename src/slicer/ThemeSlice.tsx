import { createSlice } from '@reduxjs/toolkit'
import {theme} from "../utils/types"

type themeType = theme
interface ThemeState {
    theme: themeType;
}

let localStorageTheme = window.localStorage.getItem('theme');
let savedStorage: themeType|null = null;
if (localStorageTheme !== null && 
    ["light", "dark"].includes(localStorageTheme)){
        savedStorage = localStorageTheme as themeType;
    }
const initialState: ThemeState = {
    theme: (savedStorage)? savedStorage :"light" as const,
}

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    setTheme: (state) => {
        const currentTheme = state.theme;
        let newTheme: themeType;
        if (currentTheme === "light")
            newTheme = "dark" as themeType;
        else 
            newTheme = "light" as themeType;
        window.localStorage.setItem('theme', newTheme as string)
        state.theme = newTheme; 
      }
  }
})

export const { setTheme } = themeSlice.actions;

export default themeSlice.reducer;
