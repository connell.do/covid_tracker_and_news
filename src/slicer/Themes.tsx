export type ThemeType = {
    body: string,
    textColor: string;
    header : string
}
export const lightTheme: ThemeType = {
    body: 'rgb(233, 233, 233)',
    textColor: '#000',
    header : '#FFF'
}
export const darkTheme: ThemeType = {
    body: 'rgb(19, 24, 38)',
    textColor: 'rgb(233, 233, 233)',
    header: 'rgb(33, 41, 54)'
}