import { createSlice } from '@reduxjs/toolkit'
import {mode} from "../utils/types"

interface ModeState {
    mode: mode;
}

const localStorageMode: string|null = window.localStorage.getItem('mode');
let storageMode: mode|null=null;
if (localStorageMode && (localStorageMode ==="worldwide" || localStorageMode ==="vietnam")){
    storageMode=localStorageMode;
}
const initialState: ModeState = {
    mode: (storageMode !== null)? storageMode : "worldwide",
}

const modeSlice = createSlice({
  name: 'mode',
  initialState,
  reducers: {
    setMode: (state) => {
        const currentLanguage = state.mode;
        let newMode: mode;
        if (currentLanguage === "vietnam")
            newMode = "worldwide" as mode;
        else 
            newMode = "vietnam" as mode;
        window.localStorage.setItem('mode', newMode)
        state.mode = newMode; 
      }
  }
})

export const { setMode } = modeSlice.actions;

export default modeSlice.reducer;
