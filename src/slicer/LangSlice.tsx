import { createSlice } from '@reduxjs/toolkit'
import { lang } from '../utils/types';

interface LangState {
    language: lang;
}

const localStorageLang: string|null = window.localStorage.getItem('language');
let initialLang: lang|null = null;
if (localStorageLang && (localStorageLang === "en" || localStorageLang === "vn")){
    initialLang = localStorageLang
}
const initialState: LangState = {
    language: (initialLang !== null)? initialLang : "en",
}

const langSlice = createSlice({
  name: 'lang',
  initialState,
  reducers: {
      setLanguage: (state) => {
        const currentLanguage = state.language;
        let newLanguage: lang;
        if (currentLanguage === "en")
            newLanguage = "vn";
        else 
            newLanguage = "en"
        window.localStorage.setItem('language', newLanguage)
        state.language = newLanguage; 
      }
  }
})

export const { setLanguage } = langSlice.actions;

export default langSlice.reducer;
