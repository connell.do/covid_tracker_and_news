export type mode = "worldwide"|"vietnam"

export type theme = "light"|"dark"

export type lang = "en"|"vn"