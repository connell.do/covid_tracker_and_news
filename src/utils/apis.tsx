export const COVID_CASES_HCMC =
  "https://api.zingnews.vn/public/v2/corona/getChart?loc=hochiminh";
export const COVID_CASES_HANOI =
  "https://api.zingnews.vn/public/v2/corona/getChart?loc=hanoi";
export const COVID_CASES_VIETNAM2 = 
    "https://api.zingnews.vn/public/v2/corona/getChart"
export const COVID_CASES_VIETNAM =
  "https://disease.sh/v3/covid-19/countries/vn";
export const COVID_CASES_ALL_COUNTRIES = 
    "https://disease.sh/v3/covid-19/countries";
export const COVID_CASES_WORLD_WIDE = 
    "https://disease.sh/v3/covid-19/all";
export const COVID_VACCINE_VIETNAM =
"https://api.zingnews.vn/public/v2/corona/getChart?type=vaccination";
export const COVID_CASES_PROVINCE =
  "https://api.zingnews.vn/public/v2/corona/getChart?type=province";