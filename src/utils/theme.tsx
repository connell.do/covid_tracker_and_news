export const getThemeClass = (key: string, mode: "light"|"dark"): string => {
    let newClassName = key+"-";
    if (mode === "light"){
        newClassName += "light"
    }
    else {
        newClassName += "dark"
    }
    return newClassName
}
